import argparse
import copy
import logging
import math
import random
import time

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s.%(msecs)03d %(levelname)s - %(message)s",
    datefmt="%H:%M:%S"
)
log = logging.getLogger()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", type=str, default="input.csv", help="the input file")
    parser.add_argument("--output", type=str, default="output.csv", help="the output file")
    parser.add_argument("--cluster", type=int, default=3, help="the cluster count")
    parser.add_argument("--exponent", type=int, default=2, help="the exponent for distance calculation")
    parser.add_argument("--weight", type=int, default=10000, help="the max value of random weights")
    parser.add_argument("--threshold", type=float, default=0.0000001, help="the threshold to end loops")
    args = parser.parse_args()
    input_file = args.input
    output_file = args.output
    cluster_count = args.cluster
    exponent = args.exponent
    max_weight = args.weight
    threshold = args.threshold
    log.info("Input file is %s", input_file)
    log.info("Output file is %s", output_file)
    log.info("Cluster count is %s", repr(cluster_count))
    log.info("Exponent is %s for distance calculation", repr(exponent))
    log.info("Max random weight is %s", repr(max_weight))
    log.info("Threshold is %s to end loops", repr(threshold))
    input_matrix = read_matrix(input_file)
    start = time.time()
    result = fuzzy(input_matrix, cluster_count, exponent, max_weight, threshold)
    log.info("Elapsed %.3f s", time.time() - start)
    write_matrix(result, output_file)
    log.info("Completed")


def read_matrix(path):
    log.info("Reading %s", path)
    matrix = []
    with open(str(path), "r") as file:
        for line in file:
            strings = line.strip().split(",")
            floats = []
            for string in strings:
                floats.append(float(string))
            matrix.append(floats)
    return matrix


def fuzzy(matrix, cluster_count, exponent, max_weight, threshold):
    weights = get_weights(matrix, cluster_count, max_weight)
    while True:
        original_weights = copy.deepcopy(weights)
        centers = []
        for i in range(0, cluster_count):
            center = []
            for j in range(0, len(matrix[0])):
                sum_numerator = 0.0
                sum_denominator = 0.0
                for k in range(0, len(matrix)):
                    sum_numerator += (weights[k][i] ** exponent) * matrix[k][j]
                    sum_denominator += (weights[k][i] ** exponent)
                center.append(sum_numerator / sum_denominator)
            centers.append(center)
        distances = []
        for a in range(0, len(matrix)):
            current = []
            for b in range(0, cluster_count):
                current.append(get_distance(matrix[a], centers[b]))
            distances.append(current)
        for x in range(0, cluster_count):
            for y in range(0, len(matrix)):
                distance = 0.0
                for z in range(0, cluster_count):
                    distance += (distances[y][x] / distances[y][z]) ** (2 / (exponent - 1))
                weights[y][x] = 1 / distance
        if is_close_to(weights, original_weights, threshold):
            break
    weights = normalize(weights)
    return weights


def get_weights(matrix, cluster_count, max_weight):
    result = []
    for _ in matrix:
        weights = []
        sum_weight = 0.0
        for _ in range(0, cluster_count):
            weight = random.randint(1, max_weight)
            weights.append(weight)
            sum_weight += weight
        for i in range(0, cluster_count):
            weights[i] = weights[i] / sum_weight
        result.append(weights)
    return result


def get_distance(point, center):
    if len(point) != len(center):
        return -1
    square = 0.0
    for i in range(0, len(point)):
        square += abs(point[i] - center[i]) ** 2
    return math.sqrt(square)


def is_close_to(matrix_a, matrix_b, threshold):
    for i in range(0, len(matrix_a)):
        for j in range(0, len(matrix_a[0])):
            if abs(matrix_a[i][j] - matrix_b[i][j]) > threshold:
                return False
    return True


def normalize(matrix):
    for i in range(0, len(matrix)):
        maximum = max(matrix[i])
        for j in range(0, len(matrix[0])):
            if matrix[i][j] != maximum:
                matrix[i][j] = 0
            else:
                matrix[i][j] = 1
    return matrix


def write_matrix(matrix, path):
    log.info("Writing %s", path)
    text = ""
    for line in matrix:
        for word in line:
            text += "{},".format(word)
        text = text[:-1] + "\n"
    file = open(str(path), "w")
    file.write(text)
    file.close()


if __name__ == "__main__":
    main()
